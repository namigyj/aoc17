module Main where

import qualified Day15
import qualified Day16
import qualified Day16b
import qualified Day17

main :: IO ()
main = do
  Day17.main
